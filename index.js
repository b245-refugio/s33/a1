// 3-4 Get all
fetch("https://jsonplaceholder.typicode.com/todos",
	{
		method: "GET"
	})
.then(response => response.json())
.then(result => {
	let title = result.map(data => data.title)
	console.log(title);
});

// 5-6 Get single
fetch("https://jsonplaceholder.typicode.com/todos/11",
	{
		method: "GET"
	})
.then(response => response.json())
.then(result => console.log(`The item "${result.title}" on the list has a status of ${result.completed}`));


// 6 Post create 
fetch("https://jsonplaceholder.typicode.com/todos/",
	{
		method: "POST",
		headers:{
			"Content-type": "application/json"
		},
		body:JSON.stringify({
			title: "Darna",
			completed: false,
			userId: 1
		})
	})
.then(response => response.json())
.then(result => console.log(result));

// PUT
fetch("https://jsonplaceholder.typicode.com/todos/12",
	{
		method: "PUT",
		headers:{
			"Content-type": "application/json"
		},
		body:JSON.stringify({
			title: "Darna Season 2",
			completed: false,
			userId: 1
		})
	})
.then(response => response.json())
.then(result => console.log(result));

// number 9
fetch("https://jsonplaceholder.typicode.com/todos/13",
	{
		method: "PUT",
		headers:{
			"Content-type": "application/json"
		},
		body: JSON.stringify({
			title: "Captain Barbell",
			description: "A super Hero that using a dumbell.",
			status: "Complete",
			dateCompleted: "January 31, 2000",
			userId: 2
		})

	})
.then(response => response.json())
.then(result => console.log(result));

// 10 PATCH
fetch("https://jsonplaceholder.typicode.com/todos/11",
	{
		method: "PATCH",
		headers:{
			"Content-type": "application/json"
		},
		body:JSON.stringify({
			title: "Corazon ang Huling aswang"
		})
	})
.then(response => response.json())
.then(result => console.log(result));

// 11 

fetch("https://jsonplaceholder.typicode.com/todos/1",
	{
		method: "PATCH",
		headers:{
			"Content-type": "application/json"
		},
		body: JSON.stringify({
			status: "Complete",
			dateCompleted: "January 31, 2023",
			
		})

	})
.then(response => response.json())
.then(result => console.log(result));

// 12 Delete

fetch("https://jsonplaceholder.typicode.com/todos/11",{method:"DELETE"})
.then(response => response.json())
.then(result => console.log(result));